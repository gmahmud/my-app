
import React, { Component } from 'react'
import { FlatList, Text, StyleSheet, View, ScrollView, Image } from 'react-native'
import { MapView } from "expo";


const customData = require("./demoData.json");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  row: {
    flex: 1,  
    padding: 15,
    marginBottom: 5,
    marginLeft: 10,
    backgroundColor: '#f2f2f2',
    borderRadius: 10,
    fontSize: 20,
  },
  photo: {
    height: 100,
    width: 100,
    borderRadius: 100,
  },
});



const cap = (string) => string.charAt(0).toUpperCase() + string.slice(1);

// const extractKey = ({id}) => customData[0].id.value.toString();
// keyExtractor={extractKey(customData)}
export default class App extends Component {
  
  renderItem = ({item}) => {
    return (
      <View style={styles.container}>
        <Image source={{uri: item.picture.large}}
         style={styles.photo} />
        <Text style={styles.row}>
          {cap(item.name.title)} {cap(item.name.first)} {cap(item.name.last)}
          {"\n"}{"Email: "+item.email}
          {"\n"}{"Department: "+item.dept}

        </Text>
      </View>
    )
  }
  
  render() {
    return (

      <ScrollView>
        <FlatList
          data={customData}
          renderItem={this.renderItem}
          keyExtractor={item => item.email}
        />
      </ScrollView>  
      
    );
  }
}